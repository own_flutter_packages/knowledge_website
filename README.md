Package to provide a template for a full website about the knowledge of a software developer

## Source code
Source code is available on [GitLab](https://gitlab.com/own_flutter_packages/knowledge_website).

## Getting started

Just ùse into your ``main.dart`

```dart

main()  {
  runApp(
    KnowledgeWebsite.website(
       title: title,
       headerLogo: headerLogo,
       backgroundIntro: backgroundIntro,
       links: links,
       frameworks: frameworks,
       workedAt: workedAt,
       published: published,
       stacks: stacks,
       websiteTitle: websiteTitle,
    ),
  );
}
```

## Before start`

To improve performance ratings at https://pagespeed.web.dev you should update following files that are built by flutter.

First execute:
``flutter build web --release --web-renderer html``

After having build the folder ``/example/build/web`` you should update following files.


Files that should be adjusted: 
```dart
.../knowledge_website/example/build/web/main.dart.js
.../knowledge_website/example/.dart_tool/flutter_build/.../main.dart.js
```

Replace this line:
```dart 
f.content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"
```

With:
```dart 
f.content="width=device-width, initial-scale=1.0, maximum-scale=5.0"
```

### Additional Info

### On your NGINX server

If you want to cache control your content. 

In ``/config/nginx/site-confs/default.conf`` add following location definition in ``server`` section.

    location ~* \.(js|css|jpg|jpeg|png|gif|js|css|ico|swf)$ {
                expires 1y;
                etag off;
                if_modified_since off;
                add_header Cache-Control "public, no-transform";
            }

### Get your assets preloaded

If you wish to preload your assets you can to this

- Add package ``preload: ^2.0.`` from [pub.dev](https://pub.dev/packages/preload)
- Add package ``build_runner: ^2.0.0``
- Rename temporarily ``/web/index.html`` to `ìndex.template.html`
- Add `` <!--PRELOAD-HERE-->`` where you want to have your preloads in your HTML
- Run ``flutter pub run build_runner build``
- Change for images to ``<link rel="preload" href="your_asset_path.jpg" as="image">``
