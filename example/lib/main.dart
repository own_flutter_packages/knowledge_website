import 'package:flutter/material.dart';
import 'package:knowledge_website/knowledge_website.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';

main() {
  runApp(const App());
}

class App extends StatelessWidget {
  const App({super.key});

  @override
  Widget build(BuildContext context) {
    const headerLogo = AssetImage('assets/header_logo.png');
    const backgroundIntro = AssetImage('assets/background_intro.jpg');

    return KnowledgeWebsite.website(
      title: 'Software Developer',
      headerLogo: headerLogo,
      backgroundIntro: backgroundIntro,
      websiteTitle: 'Ilja Lichtenberg (Software Developer)',
      frameworks: _frameworks(),
      links: _links(),
      published: _published(),
      workedAt: _workedAt(),
      stacks: [_stacks(), _stacksWork()],
    );
  }
}

List<KnowledgeWebsiteLink> _links() => [
      KnowledgeWebsiteLink(
        name: 'Xing',
        icon: MdiIcons.account,
        uri: Uri.https('www.xing.com', '/profile/Ilja_Lichtenberg'),
      ),
      KnowledgeWebsiteLink(
        name: 'LinkedIn',
        icon: MdiIcons.linkedin,
        uri: Uri.https('www.linkedin.com', '/in/lichtenbergil/'),
      ),
      KnowledgeWebsiteLink(
        name: 'GitLab',
        icon: MdiIcons.gitlab,
        uri: Uri.https('gitlab.com', '/own_flutter_packages/'),
      ),
      KnowledgeWebsiteLink(
        name: 'pub.dev',
        icon: MdiIcons.developerBoard,
        uri: Uri.https('pub.dev', '/publishers/ilja-lichtenberg.com/packages'),
      ),
    ];

List<KnowledgeWebsiteFramework> _frameworks() => [
      const KnowledgeWebsiteFramework(name: 'Flutter'),
      const KnowledgeWebsiteFramework(name: 'Java'),
    ];

List<KnowledgeWebsiteWorkedAt> _workedAt() => [
      KnowledgeWebsiteWorkedAt(
        nameJob: 'Development & Consulting',
        nameCompany: ['Tallence AG'],
        frameworks: [
          const KnowledgeWebsiteFramework(
            name: 'Spring Boot',
          ),
          const KnowledgeWebsiteFramework(
            name: 'Angular',
          ),
          const KnowledgeWebsiteFramework(
            name: 'OpenShift',
          ),
        ],
        descriptionJob:
            'Creating Java microservices in Kubernetes environment in context of multiple clients.',
        descriptionCompany: 'Shot term company',
        uriCompany: Uri.https('www.tallence.com', '/en'),
        uriJob: Uri.https(
          'linkedin.com',
        ),
        uriCompanyImage: Uri.https(
          'media.licdn.com',
          '/dms/image/C4E0BAQG5xT79yO9vHA/company-logo_200_200/0/1519878568357',
          {
            'e': '1680134400',
            'v': 'beta',
            't': 'vtg3PvIhCaz0ZruShfofAC4WbLsa5q2rB8KVqTtrYVk'
          },
        ),
      ),
      KnowledgeWebsiteWorkedAt(
        nameJob: 'Product development',
        nameCompany: ['Hamburg Süd', 'A.P. Moller - Maersk'],
        frameworks: [
          const KnowledgeWebsiteFramework(
            name: 'Spring',
          ),
          const KnowledgeWebsiteFramework(
            name: 'Angular',
          ),
          const KnowledgeWebsiteFramework(
            name: 'Kubernetes',
          ),
        ],
        descriptionJob:
            'Development of new product for managing the process of shipping.',
        descriptionCompany: 'Long term company',
        uriCompany: Uri.https('hamburgsud.com'),
        uriJob: Uri.https(
          'linkedin.com',
        ),
        uriCompanyImage: Uri.https(
          'media.licdn.com',
          '/dms/image/C4D0BAQHeSz7waF7RGg/company-logo_200_200/0/1533282344317',
          {
            'e': '1680134400',
            'v': 'beta',
            't': 'CI9Tp0X2ANSmnzydUhVyck29yhxgYlQu7Xwnkb1WBUo'
          },
        ),
      )
    ];

List<KnowledgeWebsitePublished> _published() => [
      KnowledgeWebsitePublished(
        name: 'Social App Template',
        description: 'You can easily create your own social media',
        installUri: Uri.https('pub.dev', '/packages/social_app_template'),
        packageUri: Uri.https('pub.dev', '/packages/social_app_template'),
        codeUri: Uri.https(
            'gitlab.com', '/own_flutter_packages/social_app_template'),
      ),
      KnowledgeWebsitePublished(
        name: 'Performance Indicator',
        description:
            'Checks internet speed connection and provides graphical output',
        installUri: Uri.https('pub.dev', '/packages/performance_indicator'),
        packageUri: Uri.https('pub.dev', '/packages/performance_indicator'),
        codeUri: Uri.https(
            'gitlab.com', '/own_flutter_packages/performance_indicator'),
      ),
      KnowledgeWebsitePublished(
        name: 'Infinite',
        description:
            'Create infinite lists and grids with preloaded media. Videos are played automatically and without performance issues.',
        installUri: Uri.https('pub.dev', '/packages/infinite'),
        packageUri: Uri.https('pub.dev', '/packages/infinite'),
        codeUri: Uri.https('gitlab.com', '/own_flutter_packages/infinite'),
      ),
    ];

List<KnowledgeWebsiteStack> _stacks() {
  return [
    KnowledgeWebsiteStack(
      name: 'Flutter',
      uri: Uri.https('stackshare.io', '/flutter'),
      image: const AssetImage('assets/icons/flutter.png'),
    ),
    KnowledgeWebsiteStack(
      name: 'Dart',
      uri: Uri.https('stackshare.io', '/dart'),
      image: const AssetImage('assets/icons/dart.png'),
    ),
    KnowledgeWebsiteStack(
      name: 'Hive',
      uri: Uri.https('docs.hivedb.dev'),
      image: const AssetImage('assets/icons/hive.png'),
    ),
    KnowledgeWebsiteStack(
      name: 'Isar',
      uri: Uri.https('isar.dev', '/de/tutorials/quickstart.html'),
      image: const AssetImage('assets/icons/isar.png'),
    ),
    KnowledgeWebsiteStack(
      name: 'Postman',
      uri: Uri.https('stackshare.io', '/postman'),
      image: const AssetImage('assets/icons/postman.png'),
    ),
    KnowledgeWebsiteStack(
      name: 'NGINX',
      uri: Uri.https('stackshare.io', '/nginx'),
      image: const AssetImage('assets/icons/ngx.png'),
    ),
    KnowledgeWebsiteStack(
      name: 'Docker',
      uri: Uri.https('stackshare.io', '/docker'),
      image: const AssetImage('assets/icons/docker.png'),
    ),
    KnowledgeWebsiteStack(
      name: 'Kubernetes',
      uri: Uri.https('stackshare.io', '/kubernetes'),
      image: const AssetImage('assets/icons/kubernetes.png'),
    ),
    KnowledgeWebsiteStack(
      name: 'GitLab',
      uri: Uri.https('stackshare.io', '/gitlab'),
      image: const AssetImage('assets/icons/gitlab.png'),
    ),
    KnowledgeWebsiteStack(
      name: 'Git',
      uri: Uri.https('stackshare.io', '/git'),
      image: const AssetImage('assets/icons/git.png'),
    ),
    KnowledgeWebsiteStack(
      name: 'IntelliJ IDEA',
      uri: Uri.https('stackshare.io', '/intellij-idea'),
      image: const AssetImage('assets/icons/intellij.png'),
    ),
    KnowledgeWebsiteStack(
      name: 'Xcode',
      uri: Uri.https('stackshare.io', '/xcode'),
      image: const AssetImage('assets/icons/xcode.png'),
    ),
    KnowledgeWebsiteStack(
      name: 'Notion',
      uri: Uri.https('stackshare.io', '/notion'),
      image: const AssetImage('assets/icons/notion.jpeg'),
    ),
    KnowledgeWebsiteStack(
      name: 'Slack',
      uri: Uri.https('stackshare.io', '/slack'),
      image: const AssetImage('assets/icons/slack.jpeg'),
    ),
  ];
}

List<KnowledgeWebsiteStack> _stacksWork() => [
      KnowledgeWebsiteStack(
        name: 'Angular/TS',
        uri: Uri.https('stackshare.io', '/angular'),
        image: const AssetImage('assets/icons/angular.jpeg'),
      ),
      KnowledgeWebsiteStack(
        name: 'Spring boot',
        uri: Uri.https('stackshare.io', '/spring-boot'),
        image: const AssetImage('assets/icons/spring.png'),
      ),
      KnowledgeWebsiteStack(
        name: 'OpenShift',
        uri: Uri.https('stackshare.io', '/red-hat-openshift'),
        image: const AssetImage('assets/icons/openshift.jpeg'),
      ),
      KnowledgeWebsiteStack(
        name: 'MongoDB',
        uri: Uri.https('stackshare.io', '/mongodb'),
        image: const AssetImage('assets/icons/mongo.png'),
      ),
      KnowledgeWebsiteStack(
        name: 'Github',
        uri: Uri.https('stackshare.io', '/github'),
        image: const AssetImage('assets/icons/github.jpeg'),
      ),
      KnowledgeWebsiteStack(
        name: 'Swagger UI',
        uri: Uri.https('stackshare.io', '/swagger-ui'),
        image: const AssetImage('assets/icons/swagger.png'),
      ),
      KnowledgeWebsiteStack(
        name: 'Jira',
        uri: Uri.https('stackshare.io', '/jira'),
        image: const AssetImage('assets/icons/jira.jpeg'),
      ),
      KnowledgeWebsiteStack(
        name: 'Confluence',
        uri: Uri.https('stackshare.io', '/confluence'),
        image: const AssetImage('assets/icons/confluence.jpeg'),
      ),
    ];
