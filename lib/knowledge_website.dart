library knowledge_website;

import 'package:flutter/material.dart';

import 'knowledge_website.dart';
import 'src/impl/knowledge_website_impl.dart';

export 'src/repo/model/knowledge_website_framework.dart'
    show KnowledgeWebsiteFramework;
export 'src/repo/model/knowledge_website_link.dart' show KnowledgeWebsiteLink;
export 'src/repo/model/knowledge_website_published.dart'
    show KnowledgeWebsitePublished;
export 'src/repo/model/knowledge_website_stack.dart' show KnowledgeWebsiteStack;
export 'src/repo/model/knowledge_website_worked_at.dart'
    show KnowledgeWebsiteWorkedAt;

part 'src/knowledge_website.dart';

/// Global constant to access KnowledgeWebsite.
// ignore: non_constant_identifier_names
final KnowledgeWebsiteInterface KnowledgeWebsite = KnowledgeWebsiteImpl();
