import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:haircut_courses_website/haircut_courses_website.dart';
import 'package:knowledge_website/knowledge_website.dart';
import 'package:knowledge_website/src/widgets/pages/qualifications/qualification_page.dart';

/// Not part of public API
class KnowledgeWebsiteImpl implements KnowledgeWebsiteInterface {
  @override
  MaterialApp website({
    required String title,
    required AssetImage headerLogo,
    required AssetImage backgroundIntro,
    required List<KnowledgeWebsiteLink> links,
    required List<KnowledgeWebsiteFramework> frameworks,
    required List<KnowledgeWebsiteWorkedAt> workedAt,
    required List<KnowledgeWebsitePublished> published,
    required List<List<KnowledgeWebsiteStack>> stacks,
    String? websiteTitle,
  }) {
    return MaterialApp(
      title: websiteTitle ?? 'My knowledge website',
      themeMode: ThemeMode.dark,
      initialRoute: '/',
      routes: {
        '/': (context) => Theme(
              data: ThemeData.dark(),
              child: LayoutBuilder(builder: (_, constraints) {
                _onInitVisitor(constraints, dev: false);

                return QualificationPage(
                  title: title,
                  links: links,
                  frameworks: frameworks,
                  workedAt: workedAt,
                  published: published,
                  stacks: stacks,
                );
              }),
            ),
        '/haircut': (context) => Theme(
              data: ThemeData(textTheme: GoogleFonts.alumniSansTextTheme()),
              child: LayoutBuilder(builder: (_, constraints) {
                _onInitVisitor(constraints, dev: true);

                return HaircutCoursesWebsite.introPage(
                  headerLogo: headerLogo,
                  backgroundIntro: backgroundIntro,
                );
              }),
            ),
      },
      color: Colors.black,
    );
  }

  void _onInitVisitor(BoxConstraints constraints, {bool dev = false}) {}
}
