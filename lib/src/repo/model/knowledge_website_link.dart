import 'package:flutter/material.dart';

class KnowledgeWebsiteLink {
  const KnowledgeWebsiteLink({
    required this.name,
    required this.icon,
    required this.uri,
  });

  final String name;
  final IconData icon;
  final Uri uri;
}
