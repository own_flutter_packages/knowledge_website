import 'package:knowledge_website/src/repo/model/knowledge_website_framework.dart';

class KnowledgeWebsiteWorkedAt {
  const KnowledgeWebsiteWorkedAt({
    required this.nameJob,
    required this.frameworks,
    required this.nameCompany,
    required this.descriptionJob,
    required this.descriptionCompany,
    required this.uriCompany,
    required this.uriCompanyImage,
    required this.uriJob,
  });

  final String nameJob;
  final List<KnowledgeWebsiteFramework> frameworks;
  final List<String> nameCompany;
  final String descriptionCompany;
  final String descriptionJob;
  final Uri uriCompany;
  final Uri uriCompanyImage;
  final Uri uriJob;
}
