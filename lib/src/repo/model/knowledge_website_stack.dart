import 'package:flutter/material.dart';

class KnowledgeWebsiteStack {
  const KnowledgeWebsiteStack({
    required this.name,
    required this.uri,
    required this.image,
  });

  final String name;
  final Uri uri;
  final AssetImage image;
}
