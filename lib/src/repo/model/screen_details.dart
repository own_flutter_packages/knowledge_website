import 'package:flutter/material.dart';

class ScreenDetails {
  final Size size;
  final bool isMobile;

  ScreenDetails({
    required this.size,
    this.isMobile = false,
  });

  double get heightScreen => size.height;

  double get widthScreen => size.width;

  double get ratio => size.aspectRatio;

  bool get isWide => ratio > 2 || ratio < 0.5;

  bool get isUltraWide => ratio > 3.5 || ratio < 0.2;

  bool get isUltraHd => size.width > 1200;

  double get heightScreenResponsive => _heightResponsive();

  double _heightResponsive() {
    final screenHeight = heightScreen;
    final factor = ((!isUltraHd && isUltraWide) ? 4 : 3);
    final adjusted = screenHeight * factor;

    final isNotMobileAndWide = !isMobile && isWide;
    final isNotMobileAndNotWide = !isMobile && !isWide;

    final isMobileDeviceException =
        (isNotMobileAndWide && (!isUltraHd || isUltraWide));
    final isBrowserDeviceException = isNotMobileAndNotWide && !isUltraHd;

    return isMobileDeviceException || isBrowserDeviceException
        ? adjusted
        : screenHeight;
  }
}
