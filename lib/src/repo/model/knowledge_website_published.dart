class KnowledgeWebsitePublished {
  const KnowledgeWebsitePublished({
    required this.name,
    required this.description,
    required this.installUri,
    required this.packageUri,
    required this.codeUri,
  });

  final String name;
  final String description;
  final Uri installUri;
  final Uri packageUri;
  final Uri codeUri;
}
