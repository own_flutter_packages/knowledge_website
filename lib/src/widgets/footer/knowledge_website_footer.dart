import 'package:flutter/material.dart';
import 'package:hovering/hovering.dart';
import 'package:knowledge_website/src/services/color_service.dart';
import 'package:knowledge_website/src/widgets/buttons/circle/circle.dart';
import 'package:knowledge_website/src/widgets/pages/qualifications/qualification_page_details.dart';

import '../pages/imprint/imprint_page.dart';

class KnowledgeWebsiteFooter extends StatelessWidget {
  const KnowledgeWebsiteFooter({
    super.key,
    required this.onTapMenu,
    required this.pageDetails,
  });

  final QualificationPageDetails pageDetails;
  final OnTapCircle onTapMenu;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => _onTap(context),
      child: Container(
        height: pageDetails.heightFooter,
        color: Colors.black,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            HoverAnimatedContainer(
              cursor: SystemMouseCursors.click,
              hoverDecoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10.0),
                color: ColorService.nintyTurkis.withOpacity(0.4),
              ),
              child: Center(
                child: Padding(
                  padding: EdgeInsets.symmetric(
                    horizontal: _paddingHorizontal() * 0.25,
                  ),
                  child: Text(
                    'Imprint',
                    style: _textStyleImprint(),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  _onTap(BuildContext ctx) {
    Navigator.of(ctx).push(
      MaterialPageRoute(
        builder: (navCtx) => ImprintPage(
          isWidescreen: pageDetails.screenDetails.isWide,
          isMobile: pageDetails.screenDetails.isMobile,
          onTapMenu: onTapMenu,
          paddingLeftPublished: _paddingHorizontal(),
        ),
      ),
    );
  }

  double _paddingHorizontal() {
    final scale = (pageDetails.screenDetails.isMobile ? 2.5 : 0.75);

    return pageDetails.paddingLeftPublished * scale;
  }

  _textStyleImprint() {
    return TextStyle(
      color: Colors.white,
      fontSize: pageDetails.screenDetails.isMobile ? 11 : 13,
    );
  }
}
