import 'package:flutter/material.dart';
import 'package:knowledge_website/src/repo/model/knowledge_website_framework.dart';
import 'package:knowledge_website/src/repo/model/screen_details.dart';

class KnowledgeWebsiteHeaderFrameworks extends StatelessWidget {
  const KnowledgeWebsiteHeaderFrameworks({
    super.key,
    required this.frameworks,
    required this.screenDetails,
  });

  final List<KnowledgeWebsiteFramework> frameworks;
  final ScreenDetails screenDetails;

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Row(
        mainAxisAlignment: _rowAlignment(),
        children: frameworks
            .map(
              (framework) => Container(
                color: Colors.transparent,
                alignment: Alignment.center,
                child: Container(
                  decoration: _frameworkDecoration(
                    isLeft: frameworks.indexOf(framework) == 0,
                  ),
                  padding: _frameworkPadding(),
                  child: Text(
                    framework.name,
                    style: _textStyleFrameworkName(),
                    maxLines: 1,
                  ),
                ),
              ),
            )
            .toList(),
      ),
    );
  }

  MainAxisAlignment _rowAlignment() => screenDetails.isMobile
      ? MainAxisAlignment.start
      : MainAxisAlignment.center;

  EdgeInsets _frameworkPadding() => EdgeInsets.only(
        left: 24.0,
        right: 24.0,
        top: screenDetails.isMobile ? 4.0 : 8.0,
        bottom: screenDetails.isMobile ? 4.0 : 8.0,
      );

  BoxDecoration _frameworkDecoration({bool isLeft = false}) {
    return BoxDecoration(
      borderRadius: isLeft
          ? const BorderRadius.only(
              topLeft: Radius.circular(10),
              bottomLeft: Radius.circular(10),
            )
          : const BorderRadius.only(
              topRight: Radius.circular(10),
              bottomRight: Radius.circular(10),
            ),
      color: Colors.black,
      border: Border.all(color: Colors.white, width: 0.7),
    );
  }

  TextStyle _textStyleFrameworkName() => TextStyle(
        fontSize: screenDetails.isMobile
            ? screenDetails.isWide
                ? 11
                : 13
            : 15,
        color: Colors.white,
      );
}
