import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:knowledge_website/src/repo/model/knowledge_website_link.dart';
import 'package:knowledge_website/src/services/color_service.dart';
import 'package:knowledge_website/src/widgets/buttons/icon_with_text/icon_with_text.dart';
import 'package:url_launcher/url_launcher.dart';

class KnowledgeWebsiteHeaderLinks extends StatelessWidget {
  const KnowledgeWebsiteHeaderLinks({
    super.key,
    required this.height,
    required this.links,
  });

  final double height;
  final List<KnowledgeWebsiteLink> links;

  @override
  Widget build(BuildContext context) {
    final maxHeight = height * 0.75;

    return Center(
      child: ConstrainedBox(
        constraints: BoxConstraints(
          maxHeight: maxHeight,
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.end,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: links
              .map(
                (link) => IconWithText(
                  icon: link.icon,
                  text: link.name,
                  iconColor: ColorService.nintyTurkis,
                  textStyleText: _textStyleText(),
                  onTap: () => _onLaunchUrl(link.uri),
                ),
              )
              .toList(),
        ),
      ),
    );
  }

  _onLaunchUrl(Uri uri) async {
    if (!await launchUrl(
      uri,
      mode: LaunchMode.externalApplication,
    )) {
      throw 'Could not launch $uri';
    }
  }

  TextStyle _textStyleText() => TextStyle(
        color: ColorService.nintyTurkis,
      );
}
