import 'package:flutter/material.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:knowledge_website/src/repo/model/knowledge_website_framework.dart';
import 'package:knowledge_website/src/repo/model/knowledge_website_link.dart';
import 'package:knowledge_website/src/widgets/buttons/circle/circle.dart';
import 'package:knowledge_website/src/widgets/header/download/knowledge_website_header_download.dart';
import 'package:knowledge_website/src/widgets/header/frameworks/knowledge_website_header_frameworks.dart';
import 'package:knowledge_website/src/widgets/pages/qualifications/qualification_page_details.dart';

import '../menu/icon/menu_icon.dart';
import 'links/knowledge_website_header_links.dart';
import 'title/knowledge_website_header_title.dart';

class KnowledgeWebsiteHeader extends StatelessWidget {
  const KnowledgeWebsiteHeader({
    super.key,
    required this.pageDetails,
    required this.title,
    required this.links,
    required this.frameworks,
    required this.onTapMenu,
  });

  final QualificationPageDetails pageDetails;
  final String title;
  final List<KnowledgeWebsiteLink> links;
  final List<KnowledgeWebsiteFramework> frameworks;
  final OnTapCircle onTapMenu;

  mobile() => Container(
        height: pageDetails.heightHeader,
        color: Colors.black,
        padding: EdgeInsets.only(
          left: pageDetails.paddingLeftPublished,
        ),
        child: StaggeredGrid.count(
          crossAxisCount: 14,
          children: [
            StaggeredGridTile.extent(
              crossAxisCellCount: 10,
              mainAxisExtent: pageDetails.heightHeader,
              child: Column(
                children: [
                  SizedBox(
                    height: pageDetails.heightHeader * 0.6,
                    child: KnowledgeWebsiteHeaderTitle(
                      title: title,
                      screenDetails: pageDetails.screenDetails,
                    ),
                  ),
                  SizedBox(
                    height: pageDetails.heightHeader * 0.4,
                    child: KnowledgeWebsiteHeaderFrameworks(
                      frameworks: frameworks,
                      screenDetails: pageDetails.screenDetails,
                    ),
                  ),
                ],
              ),
            ),
            StaggeredGridTile.extent(
              crossAxisCellCount: 2,
              mainAxisExtent: pageDetails.heightHeader,
              child: KnowledgeWebsiteHeaderDownload(
                height: pageDetails.heightHeader * 0.7,
                title: 'CV',
              ),
            ),
            StaggeredGridTile.extent(
              crossAxisCellCount: 2,
              mainAxisExtent: pageDetails.heightHeader,
              child: Container(
                alignment: Alignment.centerRight,
                padding: EdgeInsets.only(
                  right: !pageDetails.paddingLeftPublished.isNegative
                      ? pageDetails.paddingLeftPublished
                      : 0,
                ),
                child: MenuIcon(
                  size: pageDetails.heightHeader * 0.7,
                  onTap: onTapMenu,
                ),
              ),
            ),
          ],
        ),
      );

  browser() => Container(
        height: pageDetails.heightHeader,
        color: Colors.black,
        padding: EdgeInsets.only(
          left: pageDetails.paddingLeftPublished ?? 16.0,
          right: (pageDetails.paddingLeftPublished ?? 16.0) * 0.5,
        ),
        child: StaggeredGrid.count(
          crossAxisCount: 12,
          children: [
            StaggeredGridTile.extent(
              crossAxisCellCount: 4,
              mainAxisExtent: pageDetails.heightHeader,
              child: KnowledgeWebsiteHeaderTitle(
                title: title,
                screenDetails: pageDetails.screenDetails,
              ),
            ),
            StaggeredGridTile.extent(
              crossAxisCellCount: 3,
              mainAxisExtent: pageDetails.heightHeader,
              child: KnowledgeWebsiteHeaderFrameworks(
                frameworks: frameworks,
                screenDetails: pageDetails.screenDetails,
              ),
            ),
            StaggeredGridTile.extent(
              crossAxisCellCount: 4,
              mainAxisExtent: pageDetails.heightHeader,
              child: KnowledgeWebsiteHeaderLinks(
                height: pageDetails.heightHeader,
                links: links,
              ),
            ),
            StaggeredGridTile.extent(
              crossAxisCellCount: 1,
              mainAxisExtent: pageDetails.heightHeader,
              child: KnowledgeWebsiteHeaderDownload(
                height: pageDetails.heightHeader * 0.7,
                title: 'CV',
              ),
            ),
          ],
        ),
      );

  @override
  Widget build(BuildContext context) {
    return pageDetails.screenDetails.isMobile ? mobile() : browser();
  }
}
