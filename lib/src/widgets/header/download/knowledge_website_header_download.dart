import 'dart:html';

import 'package:flutter/material.dart';
import 'package:knowledge_website/src/services/color_service.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';

import '../../buttons/icon_with_text/icon_with_text.dart';

class KnowledgeWebsiteHeaderDownload extends StatelessWidget {
  const KnowledgeWebsiteHeaderDownload({
    super.key,
    required this.height,
    required this.title,
    this.alignment,
    this.wrap = false,
  });

  final double height;
  final String title;
  final Alignment? alignment;
  final bool wrap;

  @override
  Widget build(BuildContext context) {
    return Center(
      child: GestureDetector(
        onTap: _onDownload,
        child: Container(
          alignment: alignment ?? Alignment.centerRight,
          constraints: BoxConstraints(
            maxHeight: height,
          ),
          child: IconWithText(
            text: title,
            icon: MdiIcons.download,
            iconColor: ColorService.nintyYellow,
            textStyleText: _textStyleText(),
            mainAxisCount: 2,
            wrap: wrap,
          ),
        ),
      ),
    );
  }

  _onDownload() {
    _onDownloadFile(
      "https://ilja-lichtenberg.com/assets/assets/cv_lichtenberg_en_anonymous.pdf",
    );
  }

  _onDownloadFile(url) {
    AnchorElement anchorElement = AnchorElement(href: url);
    anchorElement.download = "CV";
    anchorElement.click();
  }

  TextStyle _textStyleText() => TextStyle(
        color: ColorService.nintyYellow,
        fontSize: 13,
      );
}
