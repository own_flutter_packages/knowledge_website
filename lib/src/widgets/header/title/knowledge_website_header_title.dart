import 'package:flutter/material.dart';
import 'package:knowledge_website/src/repo/model/screen_details.dart';

class KnowledgeWebsiteHeaderTitle extends StatelessWidget {
  const KnowledgeWebsiteHeaderTitle({
    super.key,
    required this.title,
    required this.screenDetails,
  });

  final String title;
  final ScreenDetails screenDetails;

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.transparent,
      alignment: Alignment.centerLeft,
      child: Text(title, style: _textStyleTitle()),
    );
  }

  TextStyle _textStyleTitle() => TextStyle(
        color: Colors.white,
        fontSize: screenDetails.isMobile
            ? screenDetails.isWide
                ? 17
                : 19
            : 29,
      );
}
