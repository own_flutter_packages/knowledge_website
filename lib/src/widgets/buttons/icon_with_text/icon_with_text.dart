import 'package:flutter/material.dart';
import 'package:hovering/hovering.dart';

typedef OnTapIconWithText = Function();

class IconWithText extends StatelessWidget {
  const IconWithText({
    super.key,
    required this.icon,
    required this.text,
    this.mainAxisCount,
    this.textStyleText,
    this.iconColor,
    this.iconSize,
    this.onTap,
    this.wrap = false,
  });

  final IconData icon;
  final String text;
  final int? mainAxisCount;
  final TextStyle? textStyleText;
  final Color? iconColor;
  final double? iconSize;
  final OnTapIconWithText? onTap;
  final bool wrap;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onTap,
      child: HoverAnimatedContainer(
        padding: const EdgeInsets.all(4.0),
        curve: Curves.linear,
        cursor: SystemMouseCursors.click,
        hoverDecoration: BoxDecoration(
          color: iconColor?.withOpacity(0.6),
          borderRadius: BorderRadius.circular(10.0),
        ),
        decoration: const BoxDecoration(
          color: Colors.transparent,
        ),
        margin: const EdgeInsets.symmetric(horizontal: 4.0),
        child: wrap
            ? Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Center(
                    child: Icon(
                      icon,
                      color: iconColor ?? Colors.white,
                      size: _iconSize(),
                    ),
                  ),
                  Center(
                    child: Text(
                      text,
                      style: textStyleText ?? _textStyleText(),
                    ),
                  ),
                ],
              )
            : Column(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  Center(
                    child: Icon(
                      icon,
                      color: iconColor ?? Colors.white,
                      size: _iconSize(),
                    ),
                  ),
                  Center(
                    child: Text(
                      text,
                      style: textStyleText ?? _textStyleText(),
                    ),
                  ),
                ],
              ),
      ),
    );
  }

  double _iconSize() => iconSize ?? 25.0;

  TextStyle _textStyleText() => const TextStyle(
        color: Colors.white,
        fontSize: 13,
      );
}
