import 'package:flutter/material.dart';
import 'package:hovering/hovering.dart';

typedef OnTapCircle = Function();

class Circle extends StatelessWidget {
  const Circle({
    Key? key,
    this.color,
    this.hoverColor,
    this.child,
    this.size,
    this.border,
    this.onTapCircle,
  }) : super(key: key);

  final Color? color;
  final Color? hoverColor;
  final Widget? child;
  final double? size;
  final Border? border;
  final OnTapCircle? onTapCircle;

  @override
  Widget build(BuildContext context) {
    final colorAdjusted = color ?? Colors.transparent;

    final Widget circle = ClipOval(
      child: GestureDetector(
        onTap: onTapCircle,
        child: HoverAnimatedContainer(
          height: size,
          width: size,
          curve: Curves.linear,
          cursor: SystemMouseCursors.click,
          padding: const EdgeInsets.only(
            left: 16.0,
            right: 16.0,
            top: 8.0,
            bottom: 8.0,
          ),
          hoverDecoration: hoverColor != null
              ? border != null
                  ? BoxDecoration(
                      color: hoverColor,
                      border: border,
                      shape: BoxShape.circle,
                    )
                  : ShapeDecoration(
                      color: hoverColor,
                      shape: const CircleBorder(),
                    )
              : null,
          decoration: border != null
              ? BoxDecoration(
                  color: colorAdjusted,
                  border: border,
                  shape: BoxShape.circle,
                )
              : ShapeDecoration(
                  color: colorAdjusted,
                  shape: const CircleBorder(),
                ),
          child: child ?? Container(),
        ),
      ),
    );

    return border != null
        ? Container(
            decoration: BoxDecoration(
              border: border,
              shape: BoxShape.circle,
            ),
            child: circle,
          )
        : circle;
  }
}
