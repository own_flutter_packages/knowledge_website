import 'package:flutter/material.dart';
import 'package:hovering/hovering.dart';
import 'package:knowledge_website/src/services/color_service.dart';
import 'package:knowledge_website/src/widgets/buttons/circle/circle.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';

class MenuIcon extends StatelessWidget {
  const MenuIcon({
    super.key,
    required this.onTap,
    this.size,
  });

  final OnTapCircle onTap;
  final double? size;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onTap,
      child: Container(
        alignment: Alignment.centerRight,
        child: HoverAnimatedContainer(
          width: size,
          height: size,
          curve: Curves.linear,
          cursor: SystemMouseCursors.click,
          hoverDecoration: BoxDecoration(
            borderRadius: BorderRadius.circular(10.0),
            color: ColorService.nintyTurkis.withOpacity(0.4),
          ),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(10.0),
            color: Colors.black,
          ),
          child: Center(
            child: Icon(
              MdiIcons.menu,
              color: ColorService.nintyTurkis,
            ),
          ),
        ),
      ),
    );
  }
}
