import 'dart:html';

import 'package:flutter/material.dart';
import 'package:knowledge_website/src/repo/model/knowledge_website_link.dart';
import 'package:knowledge_website/src/services/color_service.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:url_launcher/url_launcher.dart';

class KnowledgeWebsiteMenuDrawer extends StatelessWidget {
  const KnowledgeWebsiteMenuDrawer({
    Key? key,
    required this.links,
  }) : super(key: key);

  final List<KnowledgeWebsiteLink> links;

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;

    return Drawer(
      child: Container(
        color: Colors.black,
        child: ListView(
          padding: EdgeInsets.zero,
          children: [
            DrawerHeader(
              decoration: const BoxDecoration(
                color: Colors.black,
              ),
              child: Padding(
                padding: EdgeInsets.only(top: size.height * 0.025),
                child: Text(
                  'Ilja Lichtenberg',
                  style: TextStyle(
                    color: ColorService.nintyYellow,
                  ),
                ),
              ),
            ),
            ...links
                .map((link) => ListTile(
                      title: Row(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Padding(
                            padding: const EdgeInsets.only(right: 8.0),
                            child:
                                Icon(link.icon, size: 20, color: _iconColor()),
                          ),
                          Text(link.name, style: _textStyleMenuItem()),
                        ],
                      ),
                      onTap: () {
                        _onLaunchUrl(link.uri);
                      },
                    ))
                .toList(),
            ListTile(
              title: GestureDetector(
                onTap: _onDownloadCV,
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Padding(
                      padding: const EdgeInsets.only(right: 8.0),
                      child: Icon(MdiIcons.download,
                          size: 20, color: ColorService.nintyYellow),
                    ),
                    Text(
                      'CV',
                      style: _textStyleMenuItem(
                        color: ColorService.nintyYellow,
                      ),
                    ),
                  ],
                ),
              ),
              onTap: () {
                //_onLaunchUrl(link.uri);
              },
            ),
          ],
        ),
      ),
    );
  }

  _onLaunchUrl(Uri uri) async {
    if (!await launchUrl(
      uri,
      mode: LaunchMode.externalApplication,
    )) {
      throw 'Could not launch $uri';
    }
  }

  _onDownloadCV() {
    _onDownloadFile(
      "https://ilja-lichtenberg.com/assets/assets/cv_lichtenberg_en_anonymous.pdf",
    );
  }

  _onDownloadFile(url) {
    AnchorElement anchorElement = AnchorElement(href: url);
    anchorElement.download = "CV";
    anchorElement.click();
  }

  TextStyle _textStyleMenuItem({Color? color}) => TextStyle(
        fontSize: 13,
        color: color ?? Colors.white,
      );

  Color _iconColor() => Colors.white;
}
