import 'package:flutter/material.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:knowledge_website/src/repo/model/knowledge_website_framework.dart';
import 'package:knowledge_website/src/repo/model/knowledge_website_published.dart';
import 'package:knowledge_website/src/repo/model/knowledge_website_worked_at.dart';
import 'package:knowledge_website/src/repo/model/screen_details.dart';
import 'package:knowledge_website/src/services/size_service.dart';
import 'package:knowledge_website/src/widgets/buttons/circle/circle.dart';
import 'package:knowledge_website/src/widgets/header/knowledge_website_header.dart';
import 'package:knowledge_website/src/widgets/pages/qualifications/qualification_page_details.dart';
import 'package:knowledge_website/src/widgets/pages/qualifications/stack/knowledge_website_stacks.dart';

import '../../../repo/model/knowledge_website_link.dart';
import '../../../repo/model/knowledge_website_stack.dart';
import '../../footer/knowledge_website_footer.dart';
import '../../menu/drawer/knowledge_website_menu_drawer.dart';
import 'published/knowledge_website_published_items.dart';
import 'worked_at/knowledge_website_worked_at_items.dart';

class QualificationPage extends StatefulWidget {
  const QualificationPage({
    super.key,
    required this.title,
    required this.links,
    required this.frameworks,
    required this.workedAt,
    required this.published,
    required this.stacks,
  });

  final String title;
  final List<KnowledgeWebsiteLink> links;
  final List<KnowledgeWebsiteFramework> frameworks;
  final List<KnowledgeWebsiteWorkedAt> workedAt;
  final List<KnowledgeWebsitePublished> published;
  final List<List<KnowledgeWebsiteStack>> stacks;

  @override
  State<QualificationPage> createState() => QualificationPageState();

  browser(QualificationPageDetails pageDetails, OnTapCircle onTapMenu) =>
      SingleChildScrollView(
        physics: pageDetails.screenDetails.isWide
            ? const AlwaysScrollableScrollPhysics()
            : const NeverScrollableScrollPhysics(),
        child: StaggeredGrid.count(
          crossAxisCount: 7,
          axisDirection: AxisDirection.down,
          children: [
            StaggeredGridTile.extent(
              crossAxisCellCount: 7,
              mainAxisExtent: pageDetails.heightHeader,
              child: KnowledgeWebsiteHeader(
                pageDetails: pageDetails,
                title: title,
                links: links,
                frameworks: frameworks,
                onTapMenu: onTapMenu,
              ),
            ),
            StaggeredGridTile.extent(
              crossAxisCellCount: 4,
              mainAxisExtent: pageDetails.heightPublished,
              child: KnowledgeWebsitePublishedItems(
                published: published,
                pageDetails: pageDetails,
              ),
            ),
            StaggeredGridTile.extent(
              crossAxisCellCount: 4,
              mainAxisExtent: pageDetails.heightWorkedAt,
              child: KnowledgeWebsiteWorkedAtItems(
                workedAt: workedAt,
                pageDetails: pageDetails,
              ),
            ),
            StaggeredGridTile.extent(
              crossAxisCellCount: 3,
              mainAxisExtent: pageDetails.heightStacks,
              child: KnowledgeWebsiteStacks(
                stacks: stacks,
                pageDetails: pageDetails,
              ),
            ),
            StaggeredGridTile.extent(
              crossAxisCellCount: 7,
              mainAxisExtent: pageDetails.heightFooter,
              child: KnowledgeWebsiteFooter(
                pageDetails: pageDetails,
                onTapMenu: onTapMenu,
              ),
            ),
          ],
        ),
      );

  mobile(QualificationPageDetails pageDetails, OnTapCircle onTapMenu) =>
      ListView(
        padding: EdgeInsets.zero,
        children: [
          SizedBox(
            height: pageDetails.heightHeader,
            child: KnowledgeWebsiteHeader(
              title: title,
              links: links,
              frameworks: frameworks,
              pageDetails: pageDetails,
              onTapMenu: onTapMenu,
            ),
          ),
          KnowledgeWebsitePublishedItems(
            published: published,
            pageDetails: pageDetails,
          ),
          KnowledgeWebsiteWorkedAtItems(
            workedAt: workedAt,
            pageDetails: pageDetails,
          ),
          KnowledgeWebsiteStacks(
            stacks: stacks,
            pageDetails: pageDetails,
          ),
          SizedBox(
            height: pageDetails.heightFooter,
            child: KnowledgeWebsiteFooter(
              pageDetails: pageDetails,
              onTapMenu: onTapMenu,
            ),
          ),
        ],
      );
}

class QualificationPageState extends State<QualificationPage> {
  final keyScaffold = GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      bottom: false,
      top: false,
      maintainBottomViewPadding: false,
      left: false,
      right: false,
      child: Scaffold(
        key: keyScaffold,
        backgroundColor: Colors.black,
        resizeToAvoidBottomInset: true,
        drawerEnableOpenDragGesture: true,
        drawer: KnowledgeWebsiteMenuDrawer(links: widget.links),
        body: LayoutBuilder(
          builder: (ctx, constraints) {
            final pageDetails = QualificationPageDetails(
              screenDetails: ScreenDetails(
                size: MediaQuery.of(context).size,
                isMobile: SizeService.isMobile(constraints),
              ),
            );

            return pageDetails.screenDetails.isMobile
                ? widget.mobile(pageDetails, _onTapMenu)
                : widget.browser(pageDetails, _onTapMenu);
          },
        ),
      ),
    );
  }

  _onTapMenu() {
    final scaffold = keyScaffold.currentState;
    if (scaffold != null) {
      scaffold.openDrawer();
    }
  }
}
