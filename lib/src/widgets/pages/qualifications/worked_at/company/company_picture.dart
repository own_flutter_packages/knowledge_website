import 'dart:convert';
import 'dart:typed_data';
import 'dart:ui' as ui;

import 'package:flutter/material.dart';

import '../../../../../services/image_service.dart';

class CompanyPicture extends StatefulWidget {
  const CompanyPicture({
    super.key,
    required this.imageUrl,
    required this.height,
  });

  final String imageUrl;
  final double height;

  @override
  State<CompanyPicture> createState() => _CompanyPictureState();
}

class _CompanyPictureState extends State<CompanyPicture> {
  late Future<Uint8List?> _future;

  Future<Uint8List?> _getData() async {
    final image = await ImageService().loadImageFromWeb(widget.imageUrl);
    if (image != null) {
      final imageUint8List = await image.readAsBytes();

      if (imageUint8List == null) {
        print('err');
      }
      final converted = await convertImageFileToCustom(
        imageUint8List,
        size: widget.height.toInt(),
        titleFontSize: 15,
        addBorder: true,
        title: 'hamburg',
        titleColor: Colors.yellow,
      );

      return converted;
    }

    return null;
  }

  @override
  void initState() {
    _future = _getData();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: _future,
      builder: (context, snapshot) {
        final image = snapshot.hasData ? snapshot.data : null;

        return image != null
            ? Container(
                width: widget.height,
                height: widget.height,
                child: Image.memory(
                  image,
                ),
              )
            : Container(
                color: Colors.red,
              );
      },
    );
  }

  Future<Uint8List?> convertImageFileToCustom(
    Uint8List image, {
    String? title,
    int size = 150,
    bool addBorder = false,
    Color borderColor = Colors.white,
    double borderSize = 10,
    double titleFontSize = 13,
    Color titleColor = Colors.white,
    Color titleBackgroundColor = Colors.black,
  }) async {
    final ui.PictureRecorder pictureRecorder = ui.PictureRecorder();
    final Canvas canvas = Canvas(pictureRecorder);
    final Paint paint = Paint()..color;
    final TextPainter textPainter = TextPainter(
      textDirection: TextDirection.ltr,
    );
    final double radius = size / 2;

    //make canvas clip path to prevent image drawing over the circle
    final Path clipPath = Path();
    clipPath.addOval(Rect.fromCircle(
        center: Offset(size.toDouble() / 2, size.toDouble() / 2),
        radius: size.toDouble() / 2));
    clipPath.addRRect(RRect.fromRectAndRadius(
        Rect.fromLTWH(0, size * 8 / 10, size.toDouble(), size * 3 / 10),
        Radius.circular(100)));
    canvas.clipPath(clipPath);

    //paintImage
    final ui.Codec codec = await ui.instantiateImageCodec(image);
    final ui.FrameInfo imageFI = await codec.getNextFrame();
    paintImage(
      canvas: canvas,
      rect: Rect.fromLTWH(0, 0, size.toDouble(), size.toDouble()),
      image: imageFI.image,
    );

    if (addBorder) {
      //draw Border
      paint..color = borderColor;
      paint..style = PaintingStyle.stroke;
      paint..strokeWidth = borderSize;
      canvas.drawCircle(Offset(radius, radius), radius, paint);
    }

    if (title != null) {
      if (title.length > 9) {
        title = title.substring(0, 9);
      }
      //draw Title background
      paint..color = titleBackgroundColor;
      paint..style = PaintingStyle.fill;
      canvas.drawRRect(
          RRect.fromRectAndRadius(
              Rect.fromLTWH(0, size * 8 / 10, size.toDouble(), size * 3 / 10),
              const Radius.circular(100)),
          paint);

      //draw Title

      var encoded = utf8.encode(title ?? '');
      var decoded = utf8.decode(encoded);

      textPainter.text = TextSpan(
          text: decoded,
          style: TextStyle(
            fontSize: titleFontSize ?? radius / 4,
            fontWeight: FontWeight.bold,
            color: titleColor,
          ));
      textPainter.layout();
      textPainter.paint(
          canvas,
          Offset(radius - textPainter.width / 2,
              size * 9.5 / 10 - textPainter.height / 2));
    }

    //convert canvas as PNG bytes
    final _image = await pictureRecorder
        .endRecording()
        .toImage(size, (size * 1.1).toInt());
    final data = await _image.toByteData(format: ui.ImageByteFormat.png);

    //convert PNG bytes as BitmapDescriptor
    return data!.buffer.asUint8List();
  }
}
