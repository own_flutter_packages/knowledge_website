import 'package:flutter/material.dart';
import 'package:knowledge_website/src/repo/model/knowledge_website_framework.dart';
import 'package:knowledge_website/src/services/color_service.dart';

class KnowledgeWebsiteWorkedAtFrameworks extends StatelessWidget {
  const KnowledgeWebsiteWorkedAtFrameworks({
    super.key,
    required this.frameworks,
    required this.height,
    this.isMobile = false,
  });

  final List<KnowledgeWebsiteFramework> frameworks;
  final double height;
  final bool isMobile;

  @override
  Widget build(BuildContext context) {
    return Container(
      height: height,
      decoration: const BoxDecoration(
        color: Colors.transparent,
        borderRadius: BorderRadius.only(
          bottomLeft: Radius.circular(10),
          bottomRight: Radius.circular(10),
        ),
      ),
      child: Center(
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: frameworks
              .map(
                (framework) => Expanded(
                  child: Container(
                    alignment: Alignment.center,
                    child: Container(
                      height: height * 0.75,
                      decoration: _frameworkDecoration(),
                      margin: _frameworkMargin(),
                      child: Center(
                        child: Text(
                          framework.name,
                          style: _textStyleFrameworkName(),
                          maxLines: 1,
                        ),
                      ),
                    ),
                  ),
                ),
              )
              .toList(),
        ),
      ),
    );
  }

  EdgeInsets _frameworkPadding() =>
      EdgeInsets.symmetric(horizontal: isMobile ? 8.0 : 16.0);

  EdgeInsets _frameworkMargin() => EdgeInsets.symmetric(
        horizontal: isMobile ? 4.0 : 8.0,
      );

  BoxDecoration _frameworkDecoration() {
    return BoxDecoration(
      borderRadius: BorderRadius.circular(10.0),
      color: Colors.black,
      border: Border.all(color: ColorService.nintyTurkis, width: 1),
    );
  }

  TextStyle _textStyleFrameworkName() => TextStyle(
        color: Colors.white,
        fontSize: isMobile ? 11 : 13,
      );
}
