import 'package:flutter/material.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:hovering/hovering.dart';
import 'package:knowledge_website/src/services/color_service.dart';
import 'package:knowledge_website/src/widgets/pages/qualifications/qualification_page_details.dart';
import 'package:url_launcher/url_launcher.dart';

import '../../../../../repo/model/knowledge_website_worked_at.dart';

class KnowledgeWebsiteWorkedAtItem extends StatelessWidget {
  const KnowledgeWebsiteWorkedAtItem({
    super.key,
    required this.workedAt,
    required this.pageDetails,
    required this.height,
  });

  final QualificationPageDetails pageDetails;
  final KnowledgeWebsiteWorkedAt workedAt;
  final double height;

  @override
  Widget build(BuildContext context) {
    final heightWithInsets =
        height - (pageDetails.paddingVerticalPublished * 2);
    final heightNameJob = heightWithInsets * 0.2;
    final heightFrameworks = heightWithInsets *
        (pageDetails.screenDetails.isMobile && pageDetails.screenDetails.isWide
            ? 0.25
            : 0.15);
    final heightImage = heightWithInsets - heightNameJob - heightFrameworks;
    final heightNameCompany = heightImage;

    return GestureDetector(
      onTap: _onLaunchUrl,
      child: Center(
        child: HoverAnimatedContainer(
          height: heightWithInsets,
          cursor: SystemMouseCursors.click,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(10.0),
            color: ColorService.nintyTurkis,
            border: Border.all(color: ColorService.nintyTurkis, width: 2.0),
          ),
          hoverDecoration: BoxDecoration(
            borderRadius: BorderRadius.circular(10.0),
            color: ColorService.nintyYellow,
            border: Border.all(color: ColorService.nintyYellow, width: 4.0),
          ),
          child: StaggeredGrid.count(
            crossAxisCount: 7,
            children: [
              StaggeredGridTile.extent(
                crossAxisCellCount: 7,
                mainAxisExtent: heightNameJob,
                child: Center(
                  child: Text(
                    workedAt.nameJob,
                    style: _textStyleNameJob(),
                    maxLines: 1,
                  ),
                ),
              ),
              StaggeredGridTile.extent(
                crossAxisCellCount: 7,
                mainAxisExtent: heightImage,
                child: Container(
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10.0),
                    color: Colors.black,
                  ),
                  padding: EdgeInsets.only(right: heightImage * 0.1),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Container(
                        width: heightImage * 0.8,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(10.0),
                        ),
                        padding:
                            EdgeInsets.symmetric(horizontal: heightImage * 0.1),
                        child: ClipRRect(
                          borderRadius: BorderRadius.circular(10.0),
                          child: Image.network(
                            workedAt.uriCompanyImage.toString(),
                            fit: BoxFit.fill,
                          ),
                        ),
                      ),
                      Expanded(
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: workedAt.nameCompany
                              .map(
                                (nameCompany) => Padding(
                                  padding: EdgeInsets.symmetric(
                                    vertical: heightNameCompany * 0.02,
                                  ),
                                  child: Text(
                                    nameCompany,
                                    style: workedAt.nameCompany
                                                .indexOf(nameCompany) ==
                                            0
                                        ? _textStyleNameCompany()
                                        : _textStyleNameCompanySecond(),
                                    textAlign: TextAlign.center,
                                  ),
                                ),
                              )
                              .toList(),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              StaggeredGridTile.extent(
                crossAxisCellCount: 7,
                mainAxisExtent: heightFrameworks,
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: workedAt.frameworks
                      .map(
                        (framework) => Center(
                          child: Text(
                            framework.name,
                            style: _textStyleFrameworks(),
                          ),
                        ),
                      )
                      .toList(),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  _onLaunchUrl() async {
    if (!await launchUrl(
      workedAt.uriCompany,
      mode: LaunchMode.externalApplication,
    )) {
      throw 'Could not launch ${workedAt.uriCompany}';
    }
  }

  _textStyleNameJob() => TextStyle(
        color: Colors.black,
        fontSize: pageDetails.screenDetails.isMobile
            ? pageDetails.screenDetails.isWide
                ? 10
                : 12
            : 14,
      );

  _textStyleNameCompany() => TextStyle(
        color: Colors.white,
        fontSize: pageDetails.screenDetails.isMobile ? 11 : 15,
      );

  _textStyleNameCompanySecond() => const TextStyle(
        color: Colors.white,
        fontSize: 10,
      );

  _textStyleFrameworks() => TextStyle(
        color: Colors.black,
        fontSize: pageDetails.screenDetails.isMobile &&
                pageDetails.screenDetails.isWide
            ? 9
            : 11,
      );
}
