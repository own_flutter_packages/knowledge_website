import 'package:flutter/material.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:knowledge_website/src/repo/model/knowledge_website_worked_at.dart';
import 'package:knowledge_website/src/widgets/pages/qualifications/qualification_page_details.dart';

import 'item/knowledge_website_worked_at_item.dart';

class KnowledgeWebsiteWorkedAtItems extends StatelessWidget {
  const KnowledgeWebsiteWorkedAtItems({
    super.key,
    required this.workedAt,
    required this.pageDetails,
  });

  final List<KnowledgeWebsiteWorkedAt> workedAt;
  final QualificationPageDetails pageDetails;

  @override
  Widget build(BuildContext context) {
    final countPublished = workedAt.length;
    final heightWorkedAtItems =
        pageDetails.heightWorkedAt - pageDetails.heightTitle;

    return Container(
      color: Colors.black,
      child: StaggeredGrid.count(
        crossAxisCount: 5,
        children: [
          StaggeredGridTile.extent(
            crossAxisCellCount: 5,
            mainAxisExtent: pageDetails.heightTitle,
            child: Container(
              padding: EdgeInsets.only(left: pageDetails.paddingLeftPublished),
              alignment: const Alignment(-1.0, 0.5),
              child: Text('Worked at companies', style: _textStyleTitle()),
            ),
          ),
          StaggeredGridTile.extent(
            crossAxisCellCount: 5,
            mainAxisExtent: heightWorkedAtItems,
            child: Padding(
              padding: EdgeInsets.symmetric(
                  horizontal: pageDetails.paddingLeftPublished),
              child: StaggeredGrid.count(
                  crossAxisCount: countPublished,
                  crossAxisSpacing: 16.0,
                  children: workedAt
                      .map(
                        (workedAtItem) => StaggeredGridTile.extent(
                          crossAxisCellCount: 1,
                          mainAxisExtent: heightWorkedAtItems,
                          child: KnowledgeWebsiteWorkedAtItem(
                            height: heightWorkedAtItems,
                            workedAt: workedAtItem,
                            pageDetails: pageDetails,
                          ),
                        ),
                      )
                      .toList()),
            ),
          ),
        ],
      ),
    );
  }

  TextStyle _textStyleTitle() => TextStyle(
        color: Colors.white70,
        fontSize: pageDetails.screenDetails.isMobile
            ? pageDetails.screenDetails.isWide
                ? 15
                : 17
            : 25,
      );
}
