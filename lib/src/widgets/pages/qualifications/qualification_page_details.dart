import 'package:knowledge_website/src/repo/model/screen_details.dart';

class QualificationPageDetails {
  double get width => screenDetails.widthScreen;

  double get widthStack => screenDetails.isMobile ? width : width * (3 / 7);

  double get widthPublished =>
      screenDetails.isMobile ? width : width - widthStack;

  double get height => screenDetails.heightScreenResponsive;

  double get heightHeader => height * 0.1;

  double get heightContent => height - heightHeader - heightFooter;

  double get heightPublished => heightContent * 0.6;

  double get heightWorkedAt => heightContent - heightPublished;

  double get heightStacks => heightContent;

  double get heightStack => heightPublished;

  double get heightTitle => heightPublished * 0.15;

  double get heightFooter => screenDetails.isMobile
      ? height * 0.05
      : height * (screenDetails.isWide ? 0.025 : 0.05);

  double get paddingLeftPublished =>
      screenDetails.isMobile ? screenDetails.widthScreen * 0.05 : width * 0.05;

  double get paddingVerticalPublished => heightPublished * 0.05;

  QualificationPageDetails({
    required this.screenDetails,
  });

  final ScreenDetails screenDetails;
}
