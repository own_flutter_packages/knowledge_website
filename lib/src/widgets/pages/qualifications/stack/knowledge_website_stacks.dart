import 'package:flutter/material.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:knowledge_website/src/widgets/pages/qualifications/qualification_page_details.dart';
import 'package:knowledge_website/src/widgets/pages/qualifications/stack/item/knowledge_website_stacks_item.dart';

import '../../../../repo/model/knowledge_website_stack.dart';

class KnowledgeWebsiteStacks extends StatelessWidget {
  const KnowledgeWebsiteStacks({
    super.key,
    required this.stacks,
    required this.pageDetails,
  });

  final List<List<KnowledgeWebsiteStack>> stacks;
  final QualificationPageDetails pageDetails;

  @override
  Widget build(BuildContext context) {
    const spacing = 4.0;
    final widthStackItems = pageDetails.widthStack;
    final heightStackItems = pageDetails.heightStack - pageDetails.heightTitle;

    final heightStackItem = _heightStackItem(
      heightStackItems,
      pageDetails.paddingVerticalPublished * 2,
      spacing,
    );

    return Container(
      height: pageDetails.heightStacks,
      decoration: const BoxDecoration(color: Colors.black),
      padding: _paddingHorizontal(),
      child: StaggeredGrid.count(
        crossAxisCount: 5,
        children: stacks
            .map(
              (stackItems) => [
                StaggeredGridTile.extent(
                  crossAxisCellCount: 5,
                  mainAxisExtent: pageDetails.heightTitle,
                  child: Container(
                    color: Colors.transparent,
                    alignment: const Alignment(-1.0, 0.5),
                    child: Text(
                      _title(stackItems),
                      style: _textStyleTitle(),
                    ),
                  ),
                ),
                StaggeredGridTile.extent(
                  crossAxisCellCount: 5,
                  mainAxisExtent: heightStackItems,
                  child: Container(
                    width: widthStackItems,
                    height: heightStackItems,
                    padding: EdgeInsets.symmetric(
                        vertical: pageDetails.paddingVerticalPublished),
                    color: Colors.transparent,
                    child: StaggeredGrid.count(
                      axisDirection: AxisDirection.down,
                      crossAxisCount: _countColumns(),
                      mainAxisSpacing: spacing,
                      crossAxisSpacing: spacing,
                      children: stackItems
                          .map(
                            (stack) => StaggeredGridTile.extent(
                              mainAxisExtent: heightStackItem,
                              crossAxisCellCount: 1,
                              child: KnowledgeWebsiteStackItem(
                                stack: stack,
                                height: heightStackItem,
                                isMobile: pageDetails.screenDetails.isMobile,
                                isWidescreen: pageDetails.screenDetails.isWide,
                              ),
                            ),
                          )
                          .toList(),
                    ),
                  ),
                ),
              ],
            )
            .expand((element) => element)
            .toList(),
      ),
    );
  }

  String _title(List<KnowledgeWebsiteStack> stack) {
    return stacks.indexOf(stack) == 0 ? 'Current stack' : 'Additional stack';
  }

  double _heightStackItem(
    double heightStackItems,
    double paddingVertical,
    double spacing,
  ) {
    final countRows = _countRows();
    final spacingAll = (countRows - 1) * spacing;
    final heightStackItemsWithInsets =
        heightStackItems - paddingVertical - spacingAll;

    return heightStackItemsWithInsets / countRows;
  }

  EdgeInsets _paddingHorizontal() {
    return pageDetails.screenDetails.isMobile
        ? EdgeInsets.symmetric(horizontal: pageDetails.paddingLeftPublished)
        : EdgeInsets.zero;
  }

  int _countRows() {
    final countItems = _countItems();

    if (countItems <= 10) {
      return pageDetails.screenDetails.isMobile &&
              pageDetails.screenDetails.isWide
          ? 4
          : 3;
    }
    if (countItems <= 15) {
      return pageDetails.screenDetails.isMobile &&
              pageDetails.screenDetails.isWide
          ? 5
          : 4;
    }

    return pageDetails.screenDetails.isMobile &&
            pageDetails.screenDetails.isWide
        ? 6
        : 5;
  }

  int _countColumns() => (_countItems() / _countRows()).ceil();

  int _countItems() => stacks.isNotEmpty ? stacks.first.length : 0;

  TextStyle _textStyleTitle() => TextStyle(
        color: Colors.white70,
        fontSize: pageDetails.screenDetails.isMobile
            ? pageDetails.screenDetails.isWide
                ? 15
                : 17
            : 25,
      );
}
