import 'package:flutter/material.dart';
import 'package:hovering/hovering.dart';
import 'package:knowledge_website/src/services/color_service.dart';
import 'package:url_launcher/url_launcher.dart';

import '../../../../../repo/model/knowledge_website_stack.dart';

class KnowledgeWebsiteStackItem extends StatelessWidget {
  const KnowledgeWebsiteStackItem({
    super.key,
    required this.stack,
    required this.height,
    this.isMobile = false,
    this.isWidescreen = false,
  });

  final KnowledgeWebsiteStack stack;
  final double height;
  final bool isMobile;
  final bool isWidescreen;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: _onLaunchUrl,
      child: HoverAnimatedContainer(
        height: height,
        curve: Curves.linear,
        cursor: SystemMouseCursors.click,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(10),
        ),
        hoverDecoration: BoxDecoration(
          borderRadius: BorderRadius.circular(10),
          color: ColorService.nintyTurkis.withOpacity(0.6),
        ),
        hoverPadding: _hoverPaddingButtonLink(),
        padding: _paddingButtonLink(),
        child: Center(
          child: Column(
            children: [
              Flexible(
                flex: 2,
                child: Center(
                  child: ClipRRect(
                    borderRadius: BorderRadius.circular(10),
                    child: Image(image: stack.image),
                  ),
                ),
              ),
              Flexible(
                flex: 1,
                child: Center(
                  child: Text(stack.name, style: _textStyleName()),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  _onLaunchUrl() async {
    if (!await launchUrl(
      stack.uri,
      mode: LaunchMode.externalApplication,
    )) {
      throw 'Could not launch ${stack.uri}';
    }
  }

  EdgeInsets _paddingButtonLink() {
    return EdgeInsets.zero;
  }

  EdgeInsets _hoverPaddingButtonLink() {
    return EdgeInsets.all(height * 0.1);
  }

  _textStyleName() => TextStyle(
        color: Colors.white,
        fontSize: isMobile && isWidescreen ? 11 : 13,
      );
}
