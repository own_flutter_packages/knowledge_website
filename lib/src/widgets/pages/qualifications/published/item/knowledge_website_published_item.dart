import 'package:flutter/material.dart';
import 'package:hovering/hovering.dart';
import 'package:knowledge_website/src/repo/model/knowledge_website_published.dart';
import 'package:knowledge_website/src/services/color_service.dart';
import 'package:knowledge_website/src/widgets/pages/qualifications/qualification_page_details.dart';
import 'package:url_launcher/url_launcher.dart';

class KnowledgeWebsitePublishedItem extends StatelessWidget {
  const KnowledgeWebsitePublishedItem({
    super.key,
    required this.published,
    required this.pageDetails,
    required this.isHalfed,
    required this.heightPublishedItem,
  });

  final KnowledgeWebsitePublished published;
  final QualificationPageDetails pageDetails;
  final bool isHalfed;
  final double heightPublishedItem;

  @override
  Widget build(BuildContext context) {
    const widthBorder = 2.0;
    final heightTitle = _height() * 0.25;
    final heightContent = _height() - heightTitle - (2 * widthBorder);

    return Center(
      child: Container(
        height: _height(),
        decoration: BoxDecoration(
          color: ColorService.nintyTurkis,
          borderRadius: BorderRadius.circular(10.0),
          border: Border.all(
            color: ColorService.nintyTurkis,
            width: widthBorder,
          ),
        ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            Container(
              height: heightTitle,
              alignment: Alignment.center,
              child: Text(
                published.name,
                style: _textStyleName(),
                textAlign: TextAlign.center,
                softWrap: true,
              ),
            ),
            Container(
              height: heightContent,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10.0),
                color: Colors.black,
              ),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  Container(
                    padding: EdgeInsets.symmetric(
                        horizontal: pageDetails.widthPublished * 0.025),
                    alignment: Alignment.center,
                    child: Text(
                      published.description,
                      style: _textStyleDescription(),
                      textAlign: TextAlign.center,
                    ),
                  ),
                  Center(
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        GestureDetector(
                          onTap: () => _onLaunchUrl(published.installUri),
                          child: HoverAnimatedContainer(
                            curve: Curves.linear,
                            cursor: SystemMouseCursors.click,
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(10.0),
                              color: ColorService.nintyYellow,
                            ),
                            hoverPadding: _hoverPaddingButtonLink(),
                            padding: _paddingButtonLink(),
                            child: Text('install', style: _textStyleLink()),
                          ),
                        ),
                        GestureDetector(
                          onTap: () => _onLaunchUrl(published.codeUri),
                          child: HoverAnimatedContainer(
                            curve: Curves.linear,
                            cursor: SystemMouseCursors.click,
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(10.0),
                              color: ColorService.nintyTurkis,
                            ),
                            hoverPadding: _hoverPaddingButtonLink(),
                            padding: _paddingButtonLink(),
                            child: Text('code', style: _textStyleLink()),
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  _onLaunchUrl(Uri uri) async {
    if (!await launchUrl(
      uri,
      mode: LaunchMode.externalApplication,
    )) {
      throw 'Could not launch $uri';
    }
  }

  EdgeInsets _paddingButtonLink() {
    final height = _height();

    return EdgeInsets.only(
      left: height * 0.2,
      right: height * 0.2,
      top: height * 0.05,
      bottom: height * 0.05,
    );
  }

  EdgeInsets _hoverPaddingButtonLink() {
    final height = _height();

    return EdgeInsets.only(
      left: height * 0.24,
      right: height * 0.24,
      top: height * 0.06,
      bottom: height * 0.06,
    );
  }

  double _height() =>
      heightPublishedItem - (pageDetails.paddingVerticalPublished * 2);

  _textStyleName() => TextStyle(
        color: Colors.black,
        fontSize: pageDetails.screenDetails.isMobile
            ? pageDetails.screenDetails.isWide
                ? 14
                : 16
            : 18,
        wordSpacing: 1,
      );

  _textStyleDescription() => TextStyle(
        color: Colors.white,
        fontSize: pageDetails.screenDetails.isMobile
            ? pageDetails.screenDetails.isWide
                ? 10
                : 12
            : 14,
      );

  _textStyleLink() => TextStyle(
        color: Colors.black,
        fontSize: pageDetails.screenDetails.isMobile &&
                pageDetails.screenDetails.isWide
            ? 11
            : 13,
      );
}
