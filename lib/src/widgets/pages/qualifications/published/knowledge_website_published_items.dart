import 'package:flutter/material.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:knowledge_website/src/widgets/pages/qualifications/qualification_page_details.dart';

import '../../../../repo/model/knowledge_website_published.dart';
import 'item/knowledge_website_published_item.dart';

class KnowledgeWebsitePublishedItems extends StatelessWidget {
  const KnowledgeWebsitePublishedItems({
    super.key,
    required this.published,
    required this.pageDetails,
  });

  final List<KnowledgeWebsitePublished> published;
  final QualificationPageDetails pageDetails;

  @override
  Widget build(BuildContext context) {
    final countPublished = published.length;
    final heightPublishedItems =
        pageDetails.heightPublished - pageDetails.heightTitle;
    final heightPublishedItem = heightPublishedItems / countPublished;

    return Container(
      color: Colors.black,
      padding:
          EdgeInsets.symmetric(horizontal: pageDetails.paddingLeftPublished),
      child: StaggeredGrid.count(
        crossAxisCount: 6,
        crossAxisSpacing: pageDetails.widthPublished * 0.05,
        children: [
          StaggeredGridTile.extent(
            crossAxisCellCount: 6,
            mainAxisExtent: pageDetails.heightTitle,
            child: Container(
              color: Colors.transparent,
              alignment: const Alignment(-1.0, 0.5),
              child: Text('Published code', style: _textStyleTitle()),
            ),
          ),
          ...published.map(
            (publishedItem) {
              final index = published.indexOf(publishedItem);

              final crossAxis = index == 2 ? 6 : 3;
              final extent = heightPublishedItems / 2;

              return StaggeredGridTile.extent(
                crossAxisCellCount: crossAxis,
                mainAxisExtent: extent,
                child: KnowledgeWebsitePublishedItem(
                  published: publishedItem,
                  pageDetails: pageDetails,
                  isHalfed: index != 2,
                  heightPublishedItem: extent,
                ),
              );
            },
          ).toList(),
        ],
      ),
    );
  }

  TextStyle _textStyleTitle() => TextStyle(
        color: Colors.white70,
        fontSize: pageDetails.screenDetails.isMobile
            ? pageDetails.screenDetails.isWide
                ? 15
                : 17
            : 25,
      );
}
