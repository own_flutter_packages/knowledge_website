import 'package:flutter/material.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:knowledge_website/src/services/color_service.dart';
import 'package:knowledge_website/src/widgets/buttons/circle/circle.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';

class ImprintPage extends StatelessWidget {
  const ImprintPage({
    super.key,
    required this.isMobile,
    required this.isWidescreen,
    required this.paddingLeftPublished,
    required this.onTapMenu,
  });

  final bool isMobile;
  final bool isWidescreen;
  final double paddingLeftPublished;
  final OnTapCircle onTapMenu;

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    final heightHeader = size.height * 0.1;
    final heightContent = size.height - heightHeader;

    return Scaffold(
      backgroundColor: Colors.black,
      body: Container(
        padding: EdgeInsets.symmetric(horizontal: paddingLeftPublished),
        child: StaggeredGrid.count(
          crossAxisCount: 7,
          axisDirection: AxisDirection.down,
          children: [
            StaggeredGridTile.extent(
              crossAxisCellCount: 7,
              mainAxisExtent: heightHeader,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  SizedBox(
                    height: heightHeader * 0.3,
                    child: ElevatedButton(
                      onPressed: () => Navigator.of(context).pop(),
                      style: ButtonStyle(
                        backgroundColor: MaterialStateColor.resolveWith(
                          (_) => Colors.white,
                        ),
                        overlayColor: MaterialStateColor.resolveWith(
                          (_) => ColorService.nintyTurkis,
                        ),
                      ),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: const [
                          Icon(
                            MdiIcons.chevronLeft,
                            color: Colors.black,
                            size: 18,
                          ),
                          Padding(
                            padding: EdgeInsets.only(left: 4.0),
                            child: Text(
                              'Back',
                              style: TextStyle(
                                color: Colors.black,
                                fontSize: 16,
                                height: 1,
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
            StaggeredGridTile.extent(
              crossAxisCellCount: 7,
              mainAxisExtent: heightContent,
              child: ListView(
                physics: const NeverScrollableScrollPhysics(),
                children: [
                  Padding(
                    padding: _defaultPaddingListTile(),
                    child: Text(
                      'Imprint',
                      style: headStyle(),
                    ),
                  ),
                  Padding(
                    padding: _defaultPaddingListTile(),
                    child: Text('Ilja Lichtenberg', style: weightedStyle()),
                  ),
                  Text('+49 (0) 40 870 760 40', style: textStyle()),
                  Text('22761 Hamburg', style: textStyle()),
                  Padding(
                    padding: _defaultPaddingListTile(),
                    child: Text('Zuständiges Amtsgericht Hamburg-Altona',
                        style: weightedStyle()),
                  ),
                  Text('Max-Brauer-Allee 89', style: textStyle()),
                  Text('22765 Hamburg', style: textStyle()),
                  const Padding(
                    padding: EdgeInsets.only(top: 20.0, bottom: 10),
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }

  EdgeInsets _defaultPaddingListTile() => const EdgeInsets.only(
        top: 20.0,
        bottom: 20.0,
      );

  TextStyle headStyle() => const TextStyle(
        fontSize: 26,
        fontWeight: FontWeight.bold,
        color: Colors.white60,
      );

  TextStyle weightedStyle() => const TextStyle(
        fontSize: 16,
        fontWeight: FontWeight.bold,
        color: Color.fromRGBO(162, 36, 47, 1),
      );

  TextStyle textStyle() => const TextStyle(
        fontSize: 14,
        color: Colors.white54,
      );
}
