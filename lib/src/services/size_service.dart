import 'package:flutter/material.dart';

class SizeService {
  static bool isMobile(BoxConstraints constraints) {
    return (constraints.maxWidth) <
        (constraints.maxHeight + constraints.maxWidth * 0.3);
  }
}
