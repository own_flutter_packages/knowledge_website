import 'dart:async';
import 'dart:io';
import 'dart:ui' as ui;

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_cache_manager/flutter_cache_manager.dart';

typedef MarkImageAsNotFound = Future<void> Function(String url);
typedef IsImageMarkedAsNotFound = Function(String url);

class ImageService {
  static final ImageService _singleton = ImageService._internal();

  factory ImageService() {
    return _singleton;
  }

  ImageService._internal();

  Future<ImageService> init({
    Function(int)? before,
  }) async {
    before != null ? before(-1) : {};

    await Future.delayed(const Duration(milliseconds: 0));

    before != null ? before(1) : {};
    return _singleton;
  }

  Future<File?> loadImageFromWeb(String url) async {
    File? imageFile;

    try {
      imageFile = await _loadImageFromWeb(url);
    } catch (e) {
      if (kDebugMode) {
        print(
          '[ERROR] - when getting image from web -> ${e.toString()}',
        );
      }

      markImageAsNotFound(url);
    }

    return imageFile;
  }

  Future<File?> _loadImageFromWeb(String url) async {
    final notFound = isImageMarkedAsNotFound(url);

    if (notFound) {
      return null;
    }

    final image = await _loadCachedImage(url);

    if (image?.existsSync() ?? false) {
      return image;
    } else {
      markImageAsNotFound(url);
      return null;
    }
  }

  static Future<Size?> getSize(String url) async {
    bool notFound = isImageMarkedAsNotFound(url);

    if (notFound) return null;

    final File? image = await _loadCachedImage(url);

    if (image == null) {
      await markImageAsNotFound(url);

      return null;
    }

    final readAsBytesSync = image.readAsBytesSync();
    final decodedImage = await decodeImageFromList(readAsBytesSync);

    return Size(
      decodedImage.width.toDouble(),
      decodedImage.height.toDouble(),
    );
  }

  static Future<Uint8List> convertToLowerBytes(
    Uint8List resizedMarkerImageBytes, {
    int? targetWidth,
  }) async {
    if (resizedMarkerImageBytes.isEmpty) return resizedMarkerImageBytes;

    final ui.Codec markerImageCodec = await ui.instantiateImageCodec(
      resizedMarkerImageBytes,
      targetWidth: targetWidth ?? 300,
    );

    final ui.FrameInfo frameInfo = await markerImageCodec.getNextFrame();
    final ByteData? byteData = await frameInfo.image.toByteData(
      format: ui.ImageByteFormat.png,
    );

    if (byteData != null) {
      resizedMarkerImageBytes = byteData.buffer.asUint8List();
    }

    return resizedMarkerImageBytes;
  }

  static Future<File?> _loadCachedImage(String url) async {
    try {
      return await DefaultCacheManager().getSingleFile(url);
    } catch (e) {
      markImageAsNotFound(url);

      if (kDebugMode) {
        print(
          '[ERROR] - when getting image from cache file -> ${e.toString()}',
        );
      }

      return null;
    }
  }

  static Future<void> markImageAsNotFound(String url) async {}

  static bool isImageMarkedAsNotFound(String url) {
    return false;
  }
}
