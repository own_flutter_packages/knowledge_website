part of knowledge_website;

/// The main API interface of KnowledgeWebsite. Available through the `KnowledgeWebsite` constant.

abstract class KnowledgeWebsiteInterface {
  /// Returns full website of your knowledge
  MaterialApp website({
    required String title,
    required AssetImage headerLogo,
    required AssetImage backgroundIntro,
    required List<KnowledgeWebsiteLink> links,
    required List<KnowledgeWebsiteFramework> frameworks,
    required List<KnowledgeWebsiteWorkedAt> workedAt,
    required List<KnowledgeWebsitePublished> published,
    required List<List<KnowledgeWebsiteStack>> stacks,
    String? websiteTitle,
  });
}
